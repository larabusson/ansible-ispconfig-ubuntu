- name: Install MariaDB
  apt: name={{item}} state=installed force=yes
  with_items:
    - mariadb-client
    - mariadb-server
  tags: [database, ispconfig]

- name: MySQL to listen on all interfaces, not just localhost
  lineinfile:
    dest: /etc/mysql/mariadb.conf.d/50-server.cnf
    regexp: "^bind-address            = 127.0.0.1"
    line: "#bind-address            = 127.0.0.1"
  tags: [database, ispconfig]

- name: Set a root password in MariaDB
  expect:
    command: mysql_secure_installation
    responses:
      (.*)Enter current password for(.*): "\n"
      (.*)Set root password(.*): "y\n"
      (.*)New password(.*): "{{ mysql_password }}"
      (.*)Re-enter new password(.*): "{{ mysql_password }}"
      (.*)Remove anonymous users(.*): "y\n"
      (.*)Disallow root login remotely(.*): "y\n"
      (.*)Remove test database and access to it(.*): "y\n"
      (.*)Reload privilege tables now(.*): "y\n"
  notify: restart mysql
  tags: [database, ispconfig]

- name: Store the user and password in ~/.my.cnf
  lineinfile:
    dest: /root/.my.cnf
    insertafter: EOF
    line: "{{ item }}"
    create: True
  with_items:
    - "[client]"
    - "user = root"
    - "password = {{ mysql_password }}"
  tags: [database]

- name: Check if sendmail present
  shell: service --status-all
  register: services_list
  ignore_errors: True
  tags: [mail]

- name: Stop service sendmail and remove from update-rc.d
  systemd: name=sendmail state=stopped enabled=False
  when: "{{ 'sendmail' in services_list }}"
  tags: [mail]

- name: Debconf for postfix
  debconf:
    name: postfix
    question: "{{ item.entry }}"
    value: "{{ item.value }}"
    vtype: select
  with_items:
    - entry: mysql-server/root_password
      value: your.hostname.com
    - entry: mysql-server/root_password_again
      value: Internet Site
  tags: [mail]

- name: Install ntp postfix, Dovecot, rkhunter and binutils
  apt: name={{item}} state=installed
  with_items:
    - postfix
    - postfix-mysql
    - postfix-doc
    - libauthen-sasl-perl
    - mariadb-client
    - mariadb-server
    - openssl
    - getmail4
    - rkhunter
    - binutils
    - dovecot-imapd
    - dovecot-pop3d
    - dovecot-mysql
    - dovecot-sieve
    - dovecot-lmtpd
    - sudo
  tags: [mail]

- name: Replace lines in postfix conf
  lineinfile:
    dest: /etc/postfix/master.cf
    regexp: "{{ item.source }}"
    line: "{{ item.replaced }}"
  with_items:
    - source: "^#  -o syslog_name=postfix/submission"
      replaced: "  -o syslog_name=postfix/submission"
    - source: "^#  -o smtpd_tls_security_level=encrypt"
      replaced: "  -o smtpd_tls_security_level=encrypt"
    - source: "^#  -o smtpd_sasl_auth_enable=yes"
      replaced: "  -o smtpd_sasl_auth_enable=yes"
    - source: "^#  -o syslog_name=postfix/smtps"
      replaced: "  -o syslog_name=postfix/smtps"
    - source: "^#  -o smtpd_tls_wrappermode=yes"
      replaced: "  -o smtpd_tls_wrappermode=yes"
  tags: [mail]

- name: Add lines in postfix conf
  lineinfile:
    dest: /etc/postfix/master.cf
    regexp: "^-o smtpd_sasl_auth_enable=yes"
    insertafter: "-o smtpd_sasl_auth_enable=yes"
    line: "-o smtpd_client_restrictions=permit_sasl_authenticated,reject"
  notify: restart postfix
  tags: [mail]
