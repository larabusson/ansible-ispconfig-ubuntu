## How to deploy django site with ispconfig & mod_wsgi 

1. create vhost in ispconfig (py.fr)

2. create venv (/var/www/py.fr/venv)

```
virtualenv /var/www/py.fr/venv
```

3. copy django sources (/var/www/py.fr/limos)

4. install pip packages and run django migration commands

5. add those apache directives in vhost

```
Alias /media/ /var/www/py.fr/limos/limos/media/
Alias /static/ /var/www/py.fr/limos/limos/static/

Alias /robots.txt /var/www/py.fr/limos/limos/static/robots.txt
Alias /favicon.ico /var/www/py.fr/limos/limos/static/images/favicon.ico

WSGIDaemonProcess py.fr user=web1 group=client0 python-home=/var/www/py.fr/venv python-path=/var/www/py.fr:/var/www/py.fr/venv/lib/python2.7/site-packages
WSGIProcessGroup py.fr
WSGIScriptAlias / /var/www/py.fr/limos/limos/wsgi.py

<Directory /var/www/py.fr/limos/>
  Require all granted
</Directory>
<Directory /var/www/py.fr/limos/>
  <Files wsgi.py>
    Require all granted
  </Files>
</Directory>
```
